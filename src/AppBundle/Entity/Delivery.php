<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Delivery
 *
 * @ORM\Table(name="delivery")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DeliveryRepository")
 */
class Delivery
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="courier", type="string", length=255)
     */
    private $courier;

    /**
     * @var float
     *
     * @ORM\Column(name="shipping", type="float")
     */
    private $shipping;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float")
     */
    private $payment;

    /**
     * @var string
     *
     * @ORM\Column(name="turned", type="string", length=255)
     */
    private $turned;

    /**
     * @var int
     *
     * @ORM\Column(name="sale", type="integer", unique=true)
     */
    protected $sale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * Constructor
     */

    public function __construct()
    {

        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courier
     *
     * @param string $courier
     *
     * @return Delivery
     */
    public function setCourier($courier)
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * Get courier
     *
     * @return string
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * Set shipping
     *
     * @param float $shipping
     *
     * @return Delivery
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping
     *
     * @return float
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set payment
     *
     * @param float $payment
     *
     * @return Delivery
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set turned
     *
     * @param string $turned
     *
     * @return Delivery
     */
    public function setTurned($turned)
    {
        $this->turned = $turned;

        return $this;
    }

    /**
     * Get turned
     *
     * @return string
     */
    public function getTurned()
    {
        return $this->turned;
    }


    /**
     * Set sale
     *
     * @param integer $sale
     *
     * @return Delivery
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return integer
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Delivery
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \Application\Sonata\UserBundle\Entity\User $createdBy
     *
     * @return Delivery
     */
    public function setCreatedBy(\Application\Sonata\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
