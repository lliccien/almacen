<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\UserBundle\Entity\User as User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="denomination", type="string", length=255)
     */
    private $denomination;

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", length=255)
     */
    private $barcode;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;


    // relationships
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
     private $createdBy;

    /**
     * @ORM\OneToMany(targetEntity="ProductSale", mappedBy="Product", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $productSale;


    /**
     * Metodo Magico
     */
    public function __toString()
    {
        return $this->getDenomination();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set denomination
     *
     * @param string $denomination
     *
     * @return Product
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * Get denomination
     *
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     *
     * @return Product
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode
     *
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Product
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productMovement = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productMovement
     *
     * @param \AppBundle\Entity\Product $productMovement
     *
     * @return Product
     */
    public function addProductMovement(\AppBundle\Entity\Product $productMovement)
    {
        $this->productMovement[] = $productMovement;

        return $this;
    }

    /**
     * Remove productMovement
     *
     * @param \AppBundle\Entity\Product $productMovement
     */
    public function removeProductMovement(\AppBundle\Entity\Product $productMovement)
    {
        $this->productMovement->removeElement($productMovement);
    }

    /**
     * Get productMovement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductMovement()
    {
        return $this->productMovement;
    }

    /**
     * Set createdBy
     *
     * @param \Application\Sonata\UserBundle\Entity\User $createdBy
     *
     * @return Product
     */
    public function setCreatedBy(\Application\Sonata\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Add productSale
     *
     * @param \AppBundle\Entity\ProductSale $productSale
     *
     * @return Product
     */
    public function addProductSale(\AppBundle\Entity\ProductSale $productSale)
    {
        $this->productSale[] = $productSale;

        return $this;
    }

    /**
     * Remove productSale
     *
     * @param \AppBundle\Entity\ProductSale $productSale
     */
    public function removeProductSale(\AppBundle\Entity\ProductSale $productSale)
    {
        $this->productSale->removeElement($productSale);
    }

    /**
     * Get productSale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductSale()
    {
        return $this->productSale;
    }
}
