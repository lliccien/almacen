<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Sale
 *
 * @ORM\Table(name="sale")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SaleRepository")
 */
class Sale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numberSale", type="integer", unique=true)
     */
    private $numberSale;

    /**
     * @var int
     *
     * @ORM\Column(name="numberRuc", type="integer", unique=true)
     */
    private $numberRuc;

    /**
     * @var string
     *
     * @ORM\Column(name="typeSale", type="string", length=255)
     */
    private $typeSale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var float
     *
     * @ORM\Column(name="totalSale", type="float")
     */
    private $totalSale;

    /**
     * @var float
     *
     * @ORM\Column(name="turnedSale", type="float")
     */
    private $turnedSale;


    // RelationShip

    /**
     * @ORM\OneToMany(targetEntity="ProductSale", mappedBy="sale", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    protected $productSales;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="Sale")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentMethod", inversedBy="Sale")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id")
     */
    private $paymentMethod;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;


    /**
     * Constructor
     */

    public function __construct()
    {
        $this->productSales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * Metodo Magico
     */
    public function __toString()
    {

        return (string) $this->getNumberSale();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeSale
     *
     * @param string $typeSale
     *
     * @return Sale
     */
    public function setTypeSale($typeSale)
    {
        $this->typeSale = $typeSale;

        return $this;
    }

    /**
     * Get typeSale
     *
     * @return string
     */
    public function getTypeSale()
    {
        return $this->typeSale;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Sale
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set totalSale
     *
     * @param float $totalSale
     *
     * @return Sale
     */
    public function setTotalSale($totalSale)
    {
        $this->totalSale = $totalSale;

        return $this;
    }

    /**
     * Get totalSale
     *
     * @return float
     */
    public function getTotalSale()
    {
        return $this->totalSale;
    }

    /**
     * Set turnedSale
     *
     * @param float $turnedSale
     *
     * @return Sale
     */
    public function setTurnedSale($turnedSale)
    {
        $this->turnedSale = $turnedSale;

        return $this;
    }

    /**
     * Get turnedSale
     *
     * @return float
     */
    public function getTurnedSale()
    {
        return $this->turnedSale;
    }


    public function setProductSales(\Doctrine\Common\Collections\Collection $productSales)
    {
        foreach ($productSales as $productSale) {
            $productSale->setSale($this);
        }

        $this->productSales = $productSales;
    }


    // eliminado addProdutSale y removeProductSale

    /**
     * Get productSales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductSales()
    {
        return $this->productSales;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return Sale
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set numberSale
     *
     * @param integer $numberSale
     *
     * @return Sale
     */
    public function setNumberSale($numberSale)
    {
        $this->numberSale = $numberSale;

        return $this;
    }

    /**
     * Get numberSale
     *
     * @return integer
     */
    public function getNumberSale()
    {
        return $this->numberSale;
    }

    /**
     * Set paymentMethod
     *
     * @param \AppBundle\Entity\PaymentMethod $paymentMethod
     *
     * @return Sale
     */
    public function setPaymentMethod(\AppBundle\Entity\PaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \AppBundle\Entity\PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set createdBy
     *
     * @param \Application\Sonata\UserBundle\Entity\User $createdBy
     *
     * @return Sale
     */
    public function setCreatedBy(\Application\Sonata\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }


    /**
     * Set numberRuc
     *
     * @param integer $numberRuc
     *
     * @return Sale
     */
    public function setNumberRuc($numberRuc)
    {
        $this->numberRuc = $numberRuc;

        return $this;
    }

    /**
     * Get numberRuc
     *
     * @return integer
     */
    public function getNumberRuc()
    {
        return $this->numberRuc;
    }

}
