<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type',null, array('label' => 'Tipo de Cliente: *', 'required' => true))
                ->add('name', 'text', array('label' => 'Nombres: *'))
                ->add('lastName','text', array('label' => 'Apellidos: *'))
                ->add('birthdate','birthday', array('label' => 'fecha de Cumpleaños: *'))
                ->add('address','textarea', array('label' => 'Dirección: *'))
                ->add('email','email', array('label' => 'Correo Electónico: *'))
                ->add('phone','text', array('label' => 'Teléfono: *'))
                ->add('cellPhone','text', array('label' => 'Celular: *'))
                ->add('paymentMethod',null, array('label' => 'Método de Pago: *'))
                ->add('disease','textarea', array('label' => 'Enfermedad: *'))
                ->add('treatment','textarea', array('label' => 'Tratamiento: *'))
                ->add('note','textarea', array('label' => 'Nota: *', 'required' => false))
                //->add('createdBy')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Client'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_client';
    }


}
