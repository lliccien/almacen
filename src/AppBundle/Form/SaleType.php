<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SaleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('numberSale', 'number', array('label' =>'N° de Factura:'))
                ->add('numberRuc', 'number', array('label' =>'N° de RUC:', 'required'=>false))
                //->add('createdAt','date', array('label' =>'Fecha:', 'read_only' => true))
                ->add('client', 'entity', array('class' => 'AppBundle\\Entity\\Client',
                                                'expanded' => false,
                                                'empty_value' => 'Seleccione...', 'label' =>'Cliente:', 'required'=>true ))
                ->add('paymentMethod', 'entity', array('class' => 'AppBundle\\Entity\\PaymentMethod',
                                                       'expanded' => false,
                                                       'empty_value' => 'Seleccione...', 'label' =>'Metodo de Pago:', 'required'=>true ))
                ->add('typeSale', 'choice', array('label' =>'Tipo de Venta:','choices' => array(
                                                                                                    'VC' =>'Venta Cliente',
                                                                                                    'A' =>'Auspicio'),
                                                                                                    'empty_value' => 'Seleccione...'))
                ->add('productSales', 'collection', array(
                                                        'type' => new ProductSaleType(),
                                                        'allow_add'    => true,
                                                        'by_reference' => false,
                                                        'label' => false,
                                                    ))
                ->add('pay', 'text', array('label' =>'Monto de pago:', 'mapped'=>false, 'required'=>true))
                ->add('totalSale', null, array('label' =>'Total:'))
                ->add('turnedSale', null, array('label' =>'Vuelto:'))


        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Sale'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sale';
    }


}
