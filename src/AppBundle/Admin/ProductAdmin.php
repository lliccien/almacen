<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ProductAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('id')
            ->add('denomination', null, array('label' => 'Denominación:'))
            ->add('barcode', null, array('label' => 'Código de Barras:'))
            ->add('description', null, array('label' => 'Descripción:'))
            ->add('stock', null, array('label' => 'Existencia:'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->add('id')
            ->add('denomination', null, array('label' => 'Denominación:'))
            ->add('barcode', null, array('label' => 'Código de Barras:'))
            ->add('description', null, array('label' => 'Descripción:'))
            ->add('stock', null, array('label' => 'Existencia:'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('id')
            ->add('denomination', null, array('label' => 'Denominación:'))
            ->add('barcode', null, array('label' => 'Código de Barras:'))
            ->add('description', null, array('label' => 'Descripción:'))
            ->add('stock', null, array('label' => 'Existencia:'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            //->add('id')
            ->add('denomination', null, array('label' => 'Denominación:'))
            ->add('barcode', null, array('label' => 'Código de Barras:'))
            ->add('description', null, array('label' => 'Descripción:'))
            ->add('stock', null, array('label' => 'Existencia:'))
        ;
    }

    public function prePersist($product) {

        // trae el usuario actual logueado y lo setea en creadtedby
        $product->setCreatedBy($this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
    }


}
