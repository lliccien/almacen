<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ClientAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('id')
            ->add('name', null, array('label' => 'Nombre(s):'))
            ->add('lastName', null, array('label' => 'Apellidos(s):'))
            ->add('birthdate', null, array('label' => 'Fecha de Nacimiento:'))
            //->add('address', null, array('label' => 'Dirección:'))
            ->add('email', null, array('label' => 'Correro Electrónico:'))
            //->add('phone', null, array('label' => 'Teléfono:'))
            //->add('cellPhone', null, array('label' => 'Celular:'))
            ->add('paymentMethod', null, array('label' => 'Método de Pago'))             
            ->add('disease', null, array('label' => 'Enfermadad:'))
            //->add('treatment', null, array('label' => 'Tratamiento:'))
            //->add('note', null, array('label' => 'Nota:'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->add('id')
            ->add('name', null, array('label' => 'Nombre(s):'))
            ->add('lastName', null, array('label' => 'Apellidos(s):'))
            ->add('birthdate', null, array('label' => 'Fecha de Nacimiento:'))
            ->add('address', null, array('label' => 'Dirección:'))
            ->add('email', null, array('label' => 'Correro Electrónico:'))
            ->add('phone', null, array('label' => 'Teléfono:'))
            ->add('cellPhone', null, array('label' => 'Celular:'))
            ->add('paymentMethod', null, array('label' => 'Método de Pago'))
            ->add('disease', null, array('label' => 'Enfermadad:'))
            ->add('treatment', null, array('label' => 'Tratamiento:'))
            ->add('note', null, array('label' => 'Nota:'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('id')
            ->add('type', 'entity', array(
                'class' => 'AppBundle\Entity\ClientType',
                'label' => 'Tipo de Cliente'))            
            ->add('name', null, array('label' => 'Nombre(s):'))
            ->add('lastName', null, array('label' => 'Apellidos(s):'))
            ->add('birthdate', 'birthday', array('years' => range(1900, date('Y')), 'format' => 'dd-MMMM-yyyy'))
            ->add('address', null, array('label' => 'Dirección:'))
            ->add('email', null, array('label' => 'Correro Electrónico:'))
            ->add('phone', null, array('label' => 'Teléfono:'))
            ->add('cellPhone', null, array('label' => 'Celular:'))
            ->add('paymentMethod', 'entity', array(
                'class' => 'AppBundle\Entity\PaymentMethod',
                'choice_label' => 'denomination',
                'label' => 'Método de Pago'))            
            ->add('disease', null, array('label' => 'Enfermadad:'))
            ->add('treatment', null, array('label' => 'Tratamiento:'))
            ->add('note', null, array('label' => 'Nota:'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            //->add('id')
            ->add('name', null, array('label' => 'Nombre(s):'))
            ->add('lastName', null, array('label' => 'Apellidos(s):'))
            ->add('birthdate', null, array('label' => 'Fecha de Nacimiento:'))
            ->add('address', null, array('label' => 'Dirección:'))
            ->add('email', null, array('label' => 'Correro Electrónico:'))
            ->add('phone', null, array('label' => 'Teléfono:'))
            ->add('cellPhone', null, array('label' => 'Celular:'))
            ->add('paymentMethod', null, array('label' => 'Método de Pago'))
            ->add('disease', null, array('label' => 'Enfermadad:'))
            ->add('treatment', null, array('label' => 'Tratamiento:'))
            ->add('note', null, array('label' => 'Nota:', 'required' => false))
        ;
    }

    public function prePersist($client) {

        // trae el usuario actual logueado y lo setea en creadtedby
        $client->setCreatedBy($this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
    }

}
