<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DeliveryAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('id')
            ->add('sale', null, array('label' => 'N° de Factura'))
            ->add('courier', null, array('label' => 'Mensajero'))
            ->add('shipping', null, array('label' => 'Costo de Envio'))
            ->add('payment', null, array('label' => 'Pago'))
            ->add('turned', null, array('label' => 'Vuelto'))
            ->add('createdAt', null, array('label' => 'Vuelto'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->add('id')
            ->add('sale', null, array('label' => 'N° de Factura'))
            ->add('courier', null, array('label' => 'Mensajero'))
            ->add('shipping', null, array('label' => 'Costo de Envio'))
            ->add('payment', null, array('label' => 'Pago'))
            ->add('turned', null, array('label' => 'Vuelto'))
            ->add('createdAt',null, array('format' => 'd/m/Y H:i','label' => 'Fecha de Venta'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('id')
            ->add('sale', null, array('label' => 'N° de Factura'))
            ->add('courier', null, array('label' => 'Mensajero'))
            ->add('shipping', null, array('label' => 'Costo de Envio'))
            ->add('payment', null, array('label' => 'Pago'))
            ->add('turned', null, array('label' => 'Vuelto'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            //->add('id')
            ->add('sale', null, array('label' => 'N° de Factura'))
            ->add('courier', null, array('label' => 'Mensajero'))
            ->add('shipping', null, array('label' => 'Costo de Envio'))
            ->add('payment', null, array('label' => 'Pago'))
            ->add('turned', null, array('label' => 'Vuelto'))
            ->add('createdAt',null, array('format' => 'd/m/Y H:i','label' => 'Fecha de Venta'))
        ;
    }

    public function prePersist($delivery) {

        // trae el usuario actual logueado y lo setea en creadtedby
        $delivery->setCreatedBy($this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());

    }
}
