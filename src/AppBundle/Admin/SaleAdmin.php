<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SaleAdmin extends AbstractAdmin
{


    protected function configureRoutes(RouteCollection $collection)
    {
        //remove all routes except those, you are using in admin and you can secure by yourself
        $collection
            ->clearExcept(array(
                'list',
                'show',
                'delete',
                'batch',
                'export',
            ))
        ;
    }


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('id')
            ->add('numberSale', null, array('label' => 'Número de factura:'))
            ->add('numberRuc', null, array('label' => 'Número de RUC:'))
            ->add('typeSale', null, array('label' => 'Tipo de Venta'))
            ->add('createdAt', 'doctrine_orm_date', array('label' => 'Fecha de venta',
                'input' => 'datetime',
                'date_widget' => 'choice',
                'date_format' => ' d, m, y'))
            ->add('totalSale', null, array('label' => 'Total de Venta'))
            ->add('turnedSale', null, array('label' => 'Vuelto'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->add('id')
            ->add('numberSale', null, array('label' => 'Número de factura'))
            ->add('numberRuc', null, array('label' => 'Número de RUC:'))
            ->add('typeSale', null, array('label' => 'Tipo de Venta', 'template' => ':Admin:field_list_typeSale.html.twig'))
            ->add('createdAt',null, array('format' => 'd/m/Y H:i','label' => 'Fecha de Venta'))
            ->add('totalSale', null, array('label' => 'Total de Venta'))
            ->add('turnedSale', null, array('label' => 'Vuelto'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    //'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('id')
            //->with('Datos de la Venta', array('class' => 'col-md-6'))
                ->add('numberSale', null, array('label' => 'Nombre(s):'))
                ->add('numberRuc', null, array('label' => 'Número de RUC:', 'require' => false))
                ->add('typeSale')
                ->add('createdAt')
                ->add('totalSale')
                ->add('turnedSale')
            //->end()
            /*
            ->with('Detalle de la Venta', array('class' => 'col-md-6'))
            ->add('productSales', 'sonata_type_collection', array( 'by_reference' => false
            ), array(
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->end()
            */


        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            //->add('id')
            ->add('numberSale', null, array('label' => 'Número de factura:'))
            ->add('numberRuc', null, array('label' => 'Número de RUC:'))
            ->add('typeSale', null, array('label' => 'Tipo de venta:', 'template' => ':Admin:field_show_typeSale.html.twig'))
            ->add('productSales', null, array('label' => 'Productos:'))
            ->add('createdAt', null, array('label' => 'Fecha de venta:'))
            ->add('totalSale', null, array('label' => 'Total de venta:'))
            ->add('turnedSale', null, array('label' => 'Vuelto:'))
        ;
    }


    public function prePersist($sale) {

        // trae el usuario actual logueado y lo setea en creadtedby
        $sale->setCreatedBy($this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());

    }


    function preRemove($sale){


        $productSales = $sale->getProductSales();

        foreach ($productSales as $productSale) {

            $amount = $productSale->getAmount();
            $stock = $productSale->getProduct()->getStock();
            $total = $stock + $amount;
            $productSale->getProduct()->setStock($total);

        }

    }

    public function reCalculator($sale){

    }

}
