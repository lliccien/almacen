<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProductSale;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Productsale controller.
 *
 * @Route("productsale")
 */
class ProductSaleController extends Controller
{
    /**
     * Lists all productSale entities.
     *
     * @Route("/", name="productsale_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $productSales = $em->getRepository('AppBundle:ProductSale')->findAll();

        return $this->render('productsale/index.html.twig', array(
            'productSales' => $productSales,
        ));
    }

    /**
     * Creates a new productSale entity.
     *
     * @Route("/new", name="productsale_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $productSale = new Productsale();
        $form = $this->createForm('AppBundle\Form\ProductSaleType', $productSale, array('show_legend' => false));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($productSale);
            $em->flush($productSale);

            return $this->redirectToRoute('productsale_show', array('id' => $productSale->getId()));
        }

        return $this->render('productsale/new.html.twig', array(
            'productSale' => $productSale,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a productSale entity.
     *
     * @Route("/{id}", name="productsale_show")
     * @Method("GET")
     */
    public function showAction(ProductSale $productSale)
    {
        $deleteForm = $this->createDeleteForm($productSale);

        return $this->render('productsale/show.html.twig', array(
            'productSale' => $productSale,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing productSale entity.
     *
     * @Route("/{id}/edit", name="productsale_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ProductSale $productSale)
    {
        $deleteForm = $this->createDeleteForm($productSale);
        $editForm = $this->createForm('AppBundle\Form\ProductSaleType', $productSale);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('productsale_edit', array('id' => $productSale->getId()));
        }

        return $this->render('productsale/edit.html.twig', array(
            'productSale' => $productSale,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a productSale entity.
     *
     * @Route("/{id}", name="productsale_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ProductSale $productSale)
    {
        $form = $this->createDeleteForm($productSale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($productSale);
            $em->flush($productSale);
        }

        return $this->redirectToRoute('productsale_index');
    }

    /**
     * Creates a form to delete a productSale entity.
     *
     * @param ProductSale $productSale The productSale entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProductSale $productSale)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('productsale_delete', array('id' => $productSale->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
