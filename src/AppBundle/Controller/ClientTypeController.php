<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ClientType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Clienttype controller.
 *
 * @Route("clienttype")
 */
class ClientTypeController extends Controller
{
    /**
     * Lists all clientType entities.
     *
     * @Route("/", name="clienttype_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clientTypes = $em->getRepository('AppBundle:ClientType')->findAll();

        return $this->render('clienttype/index.html.twig', array(
            'clientTypes' => $clientTypes,
        ));
    }

    /**
     * Creates a new clientType entity.
     *
     * @Route("/new", name="clienttype_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $clientType = new Clienttype();
        $form = $this->createForm('AppBundle\Form\ClientTypeType', $clientType, array('show_legend' => false));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($clientType);
            $em->flush($clientType);

            return $this->redirectToRoute('clienttype_index');
        }

        return $this->render('clienttype/new.html.twig', array(
            'clientType' => $clientType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a clientType entity.
     *
     * @Route("/{id}", name="clienttype_show")
     * @Method("GET")
     */
    public function showAction(ClientType $clientType)
    {
        $deleteForm = $this->createDeleteForm($clientType);

        return $this->render('clienttype/show.html.twig', array(
            'clientType' => $clientType,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing clientType entity.
     *
     * @Route("/{id}/edit", name="clienttype_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ClientType $clientType)
    {
        $deleteForm = $this->createDeleteForm($clientType);
        $editForm = $this->createForm('AppBundle\Form\ClientTypeType', $clientType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clienttype_edit', array('id' => $clientType->getId()));
        }

        return $this->render('clienttype/edit.html.twig', array(
            'clientType' => $clientType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clientType entity.
     *
     * @Route("/{id}", name="clienttype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ClientType $clientType)
    {
        $form = $this->createDeleteForm($clientType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clientType);
            $em->flush($clientType);
        }

        return $this->redirectToRoute('clienttype_index');
    }

    /**
     * Creates a form to delete a clientType entity.
     *
     * @param ClientType $clientType The clientType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ClientType $clientType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clienttype_delete', array('id' => $clientType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
