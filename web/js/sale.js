// Obtiene la ul que contiene la colección de etiquetas


var collectionHolder = $('tbody.productsale');
//console.log( collectionHolder );


// configura una enlace "Agregar una etiqueta"
var $addTagLink = $('<a href="#" class="btn btn-primary add_tag_link"><i class="fa fa-plus" aria-hidden="true"></i> Añadir producto</a>');
var $newLinkLi = $('<tr></tr>').append($addTagLink);




jQuery(document).ready(function() {
    // Añade el ancla "Agregar una etiqueta" y las etiquetas li y ul
    collectionHolder.append($newLinkLi);

    // cuenta las entradas actuales en el formulario (p. ej. 2),
    // la usa como índice al insertar un nuevo elemento (p. ej. 2)
    collectionHolder.data('index', collectionHolder.find(':input').length);



    $('#calular').prop("disabled", true);

    $addTagLink.on('click', function(e) {
        // evita crear el enlace con una "#" en la URL
        e.preventDefault();

        // añade una nueva etiqueta form (ve el siguiente bloque de código)
        addTagForm(collectionHolder, $newLinkLi);


        $('#calular').prop("disabled", false);
        $('input[id $= _amountPrice]').prop('readonly', true);
    });

    //var $calcular = $('#calcular');

    $('input[id $= _totalSale]').prop('readonly', true);
    $('input[id $= _turnedSale]').prop('readonly', true);

    $('#vuelto').prop("disabled", true);

    $( "#calular" ).click(function() {

        var amount = $('input[id $= _amount]');
        var price = $('input[id $= _price');
        var amountPrice = $('input[id $= _amountPrice]');

        var totalSale = $('input[id $= _totalSale]');

        var total = 0;

        for (var i = 0, length = amount.length; i < length; i++) {

            var multi = parseFloat(amount[i].value) * parseFloat(price[i].value);

            amountPrice[i].value = multi;

            total = total + parseFloat(amountPrice[i].value);



        }
        //console.log(total);
        //console.log(totalSale);
        totalSale.val(total);
        $('#vuelto').prop("disabled", false);
        $('#sale_pay').focus();

    });

    //console.log($('input[id $= _totalSale]'));
    //console.log($('#sale_pay'));

    $( "#vuelto" ).click(function() {



        var total = $('input[id $= _totalSale]').val();
        var paga = $('#sale_pay').val();

        var cambio = parseFloat(paga) - parseFloat(total);

        $('input[id $= _turnedSale]').val(cambio);

        //console.log(total, paga, cambio);


    });



});


function addTagForm(collectionHolder, $newLinkLi) {
    // Obtiene los datos del prototipo explicado anteriormente
    var prototype = collectionHolder.data('prototype');

    // Consigue el nuevo índice
    var index = collectionHolder.data('index');

    // Sustituye el '__name__' en el prototipo HTML para que
    // en su lugar sea un número basado en cuántos elementos hay
    var newForm = prototype.replace(/__name__/g, index);

    // Incrementa en uno el índice para el siguiente elemento
    collectionHolder.data('index', index + 1);

    // Muestra el formulario en la página en un elemento li,
    // antes del enlace 'Agregar una etiqueta'
    var $newFormLi = $('<tr></tr>').append(newForm);
    $newLinkLi.before($newFormLi);

}

